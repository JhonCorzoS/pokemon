const numeroPokemon = 905;
const numeroImagenes = 4;
const tipos = 18;
const generaciones = 8;
const botonBuscar = document.querySelector(".btn-buscar");
const output = document.querySelector(".output");
let tipoActual = null;
let generacionActual = null;
let pokemonIds = [];
let pagina = 0;

window.addEventListener("load", async () => {
  crearCarrusel();
  let types = await fetch("https://pokeapi.co/api/v2/type").then(res => res.json());
  const selectorTipos = document.querySelector(".tipos");
  selectorTipos.addEventListener("change", (e) => {
    if(e.target.value == "null") tipoActual = null;
    else tipoActual = e.target.value;
  });
  for(let i = 0; i < tipos; i++) {
    let nombreTipo = `${types["results"][i]["name"]}`;
    selectorTipos.innerHTML += `<option value="${nombreTipo}">${nombreTipo}</option>`;
  }

  let generations = await fetch("https://pokeapi.co/api/v2/generation").then(res => res.json());
  const selectorGeneraciones = document.querySelector(".generaciones");
  selectorGeneraciones.addEventListener("change", (e) => {
    if(e.target.value == "null") generacionActual = null;
    else generacionActual = e.target.value;
  });
  for(let i = 0; i < generaciones; i++) {
    let nombreGeneracion = `${generations["results"][i]["name"]}`;
    selectorGeneraciones.innerHTML += `<option value="${nombreGeneracion}">${nombreGeneracion}</option>`; 
  }
});

const crearCarrusel = () => {
  const carrusel = document.querySelector(".carousel-inner");
  const fragmento = document.createDocumentFragment();
  for(let i = 0; i < generaciones; i++) {
    const item = document.createElement("DIV");
    item.classList.add("carousel-item");
    if(i == 0) {
      item.classList.add("active");
    }
    item.innerHTML = `<div class="col-md-3">
                        <div class="w-100 d-flex justify-content-center align-content-center hem p-1">
                          <img src="img/${regiones[i]["nombre"].toLowerCase()}.png" class="img-fluid rounded-circle" alt="...">
                        </div>
                        <div class="text-center">
                          <h5 class="m-3">${regiones[i]["nombre"]}</h5>
                          <p class="fs-6 fst-italic">${regiones[i]["descripcion"]}</p>
                        </div>
                      </div>`;
    fragmento.appendChild(item);
  }
  carrusel.appendChild(fragmento);
  let items = document.querySelectorAll('.carousel .carousel-item');
  items.forEach((el) => {
      const minPerSlide = 4;
      let next = el.nextElementSibling;
      for (var i=1; i<minPerSlide; i++) {
          if (!next) {
              // wrap carousel by using first child
            next = items[0];
          }
          let cloneChild = next.cloneNode(true);
          el.appendChild(cloneChild.children[0]);
          next = next.nextElementSibling;
      }
  });
}

botonBuscar.addEventListener("click", async () => {
  pokemonIds = [];
  const inputNombre = document.querySelector(".nombrePokemon");
  if(inputNombre.value == "") {
    if(generacionActual != null && tipoActual != null) {
      let jsonGeneracion = await fetch(`https://pokeapi.co/api/v2/generation/${generacionActual}`).then(res => res.json());
      let pokemon = jsonGeneracion["pokemon_species"];
      let pokemonGeneracion = [];
      for(let i = 0; i < pokemon.length; i++) {
        let id = pokemon[i]["url"].substring(42);
        id = parseInt(id.substring(0, id.length - 1));
        pokemonGeneracion.push(id);
      }

      let jsonTipo = await fetch(`https://pokeapi.co/api/v2/type/${tipoActual}`).then(res => res.json());
      pokemon = jsonTipo["pokemon"];
      for(let i = 0; i < pokemon.length; i++) {
        let id = pokemon[i]["pokemon"]["url"].substring(34);
        id = parseInt(id.substring(0, id.length - 1));
        if(pokemonGeneracion.includes(id)) {
          pokemonIds.push(id);
        }
      }
    }
    else if(generacionActual != null) {
      let jsonGeneracion = await fetch(`https://pokeapi.co/api/v2/generation/${generacionActual}`).then(res => res.json());
      let pokemon = jsonGeneracion["pokemon_species"];
      for(let i = 0; i < pokemon.length; i++) {
        let id = pokemon[i]["url"].substring(42);
        id = parseInt(id.substring(0, id.length - 1));
        pokemonIds.push(id);
      }
    }
    else if(tipoActual != null) {
      let jsonTipo = await fetch(`https://pokeapi.co/api/v2/type/${tipoActual}`).then(res => res.json());
      let pokemon = jsonTipo["pokemon"];
      for(let i = 0; i < pokemon.length; i++) {
        let id = pokemon[i]["pokemon"]["url"].substring(34);
        id = parseInt(id.substring(0, id.length - 1));
        pokemonIds.push(id);
      }
    } 
    else {
      for(let i = 0; i < numeroPokemon; i++) {
        pokemonIds.push(i + 1);
      }
    }
  }
  else {
    let jsonPokemon = await fetch(`https://pokeapi.co/api/v2/pokemon/${inputNombre.value.toLowerCase()}`).then(res => res.json()).catch((error) => {return null});
    if(jsonPokemon != null) {
      let id = parseInt(jsonPokemon["id"]);
      pokemonIds.push(id);
    }
  }
  while(output.firstChild) {
    output.removeChild(output.firstChild);
  }
  crearPaginacion();
  mostrarPokemon(pagina);
});

function cambiarPagina(e) {
  if(e.target.innerText === "Previous") {
    if(pagina == 0) return;
    pagina--;
  }
  else {
    if(pagina == Math.ceil(pokemonIds.length / numeroImagenes) - 1) return;
    pagina++;
  } 
  mostrarPokemon(pagina);
}

const crearPaginacion = () => {
  pagina = 0;
  let paginacion = document.createElement("DIV");
  paginacion.classList.add("row");
  paginacion.innerHTML = `<div class="col-12 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                              <ul class="pagination">
                                <li class="page-item"><a class="page-link">Previous</a></li>
                                <li class="page-item"><a class="page-link">Next</a></li>
                              </ul>
                            </nav>
                          </div>`;
  output.appendChild(paginacion);
  let botonesPagina = document.querySelectorAll(".page-link");
  for(let page of botonesPagina) {
    page.addEventListener("click", cambiarPagina);
  }
}

const obtenerPokemon = (id) => {
  let url = `https://pokeapi.co/api/v2/pokemon/${id}`;
  return fetch(url).then(res => res.json());
}

const mostrarPokemon = async (pagina) => {
  let contenedorAntiguo = document.querySelector(".cards-pokemon");
  if(contenedorAntiguo != null) {
    output.removeChild(contenedorAntiguo);
  }
  let l = pagina * numeroImagenes;
  let r = Math.min(l + numeroImagenes, pokemonIds.length);
  let contenedor = document.createElement("DIV");
  contenedor.classList.add("row");
  contenedor.classList.add("cards-pokemon");
  for(let i = l; i < r; i++) {
    let respuesta = await obtenerPokemon(pokemonIds[i]);
    let img = respuesta["sprites"]["other"]["official-artwork"]["front_default"];
    let nombre = respuesta["name"][0].toUpperCase() + respuesta["name"].substring(1);
    contenedor.innerHTML += `<div class="col-12 col-sm-6 col-md-3">
                              <div class="card justify-content-center text-center p-2 m-1 bg-danger">
                                <div class="d-flex align-items-end">
                                  <div class="bg-success rounded-circle m-1 circle-1"></div>
                                  <div class="bg-primary rounded-circle m-1 circle-2"></div>
                                  <div class="bg-warning rounded-circle m-1 circle-2"></div>
                                  <input type="number" hidden="true" value="${pokemonIds[i]}">
                                </div>

                                <img class="card-img-top border border-5 border-dark bg-light" src="${img}"
                                  alt="pokemon_img">
                                <div class="d-flex">
                                  <div class="bg-warning rounded-circle m-1 circle-1"></div>
                                  <div class="bg-success rounded-3 m-1 rec"></div>
                                  <div class="bg-primary rounded-3 m-1 rec"></div>
                                </div>
                                <div class="card-body m-0 p-0">
                                  <h4 class="card-title pb-3 text-light">${nombre}</h4>
                                  <form action="html/pokedexPage.html">
                                    <input class="form-control" type="number" placeholder="Hidden input" id="id" name="id" value="${pokemonIds[i]}" hidden="true">
                                    <button type="submit" class="btn btn-dark mb-3">Ver más</button>
                                  </form>
                                </div>
                              </div>
                            </div>`;
  }
  output.appendChild(contenedor);
}

const regiones = [
    {
      nombre : "Kanto",
      descripcion : `Es una región del mundo Pokémon situada al este de
      Johto y al sur de Sinnoh. Su paisaje está inspirado en la zona de Japón del
      mismo nombre, la región de Kantō.`
    },
    {
      nombre : "Johto",
      descripcion : `Es una región del mundo Pokémon situada al oeste de
      Kanto. Su paisaje está inspirado en la zona de Japón llamada región de Kinki
      y el oeste de la región de Tōkai.`
    },
    {
      nombre : "Hoenn",
      descripcion : `Es la región del mundo Pokémon donde se desarrolla la
      trama de los videojuegos Pokémon Rubí, Zafiro y Esmeralda y sus remakes,
      Pokémon Rubí Omega y Pokémon Zafiro Alfa.`
    },
    {
      nombre : "Sinnoh",
      descripcion : `Es una región del mundo Pokémon. Está al norte de
      Kanto. Su paisaje está inspirado en la zona de Japón Hokkaidō, la mitad sur
      de la isla rusa Sajalín y la isla de Kunashir.`
    },
    {
      nombre : "Teselia",
      descripcion : `Es la región en donde se desarrolla la trama de los
      videojuegos Pokémon Negro y Pokémon Blanco, y posteriormente también es
      escenario de los videojuegos Pokémon Negro 2 y Pokémon Blanco 2.`
    },
    {
      nombre : "Kalos",
      descripcion : `Es la región del mundo Pokémon donde se desarrolla la
      trama de los videojuegos Pokémon X y Pokémon Y en la sexta generación.`
    },
    {
      nombre : "Alola",
      descripcion : `Es una región del mundo Pokémon compuesta por cuatro
      islas naturales: Melemele, Akala, Ula-Ula, y Poni, además de una pequeña
      isla artificial: el Paraíso Aether.`
    },
    {
      nombre : "Galar",
      descripcion : `Es una región del mundo Pokémon donde se desarrollará
      la trama de los videojuegos Pokémon Espada y Pokémon Escudo de la octava
      generación.`
    }
]

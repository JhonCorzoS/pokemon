let cntAciertos = 0;
let cntIntentos = 0;
let target = 0;
const numeroImagenes = 4;
const numeroPokemon = 905;
const generaciones = [
  {
    indiceMaxPokedex : 151,
    nombre : "Generación I"
  },
  {
    indiceMaxPokedex : 251,
    nombre : "Generación II"
  },
  {
    indiceMaxPokedex : 386,
    nombre : "Generación III"
  },
  {
    indiceMaxPokedex : 493,
    nombre : "Generación IV"
  },
  {
    indiceMaxPokedex : 649,
    nombre : "Generación V"
  },
  {
    indiceMaxPokedex : 721,
    nombre : "Generación VI"
  },
  {
    indiceMaxPokedex : 809,
    nombre : "Generación VII"
  },
  {
    indiceMaxPokedex : 905,
    nombre : "Generación VIII"
  }
]

const boton = document.querySelector(".btn");

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const obtenerPokemon = (id) => {
  let url = `https://pokeapi.co/api/v2/pokemon/${id}`;
  return fetch(url).then(res => res.json());
}

const mostrarErrores = () => {
  for(let i = 0; i < numeroImagenes; i++) {
    let img = document.querySelector(`.img-${i}`);
    img.removeEventListener("click", escoger);
    if(i == target) continue;
    const elemento = document.querySelector(`.img-${i}`);
    elemento.setAttribute("src", "../img/error.png");
  }
}

function escoger(e) {
  mostrarErrores(target);
  if(e.target.classList.contains(`img-${target}`)) {
    cntAciertos++;
  }
  const points = document.querySelector(".points");
  points.innerText = cntAciertos.toString();
  boton.classList.remove("disabled");
}

const cargarImagenes = async () => {
  const contenedor = document.querySelector(".contenedorImagenes");
  while(contenedor.firstChild) {
    contenedor.removeChild(contenedor.firstChild);
  }
  const nombre = document.querySelector(".nombre");
  const generacion = document.querySelector(".generacion");
  const fragmento = document.createDocumentFragment();
  target = getRandomInt(1000007) % 4;
  
  for(let i = 0; i < numeroImagenes; i++) {
    let id = getRandomInt(numeroPokemon - 1) + 1;
    let respuesta = await obtenerPokemon(id);
    let url = respuesta["sprites"]["other"]["official-artwork"]["front_default"];
    const columna = document.createElement("DIV");
    columna.classList.add("col-6");
    columna.classList.add("col-lg-3");
    columna.classList.add("p-3");
    columna.innerHTML += `<div class="container-fluid">
                            <div class="container-fluid border border-2 bg-body trivia">
                              <img src="${url}" alt=${id} class="img-${i} img-fluid w-100">
                            </div>
                          </div>`;
    if(i == target) {
      nombre.innerText = respuesta["name"][0].toUpperCase() + respuesta["name"].substring(1);
      for(let i = 0; i < generaciones.length; i++) {
        if(id <= generaciones[i].indiceMaxPokedex) {
          generacion.innerText = generaciones[i].nombre;
          break;      
        }
      }
    }
    fragmento.appendChild(columna);
  }
  contenedor.appendChild(fragmento);
  for(let i = 0; i < numeroImagenes; i++) {
    let img = document.querySelector(`.img-${i}`);
    img.addEventListener("click", escoger);
  } 
}

const cargarSuccess = () => {
  const contenedor = document.querySelector(".contenedorImagenes");
  while(contenedor.firstChild) {
    contenedor.removeChild(contenedor.firstChild);
  }

  const fragmento = document.createDocumentFragment();
  
  for(let i = 0; i < numeroImagenes; i++) {
    const columna = document.createElement("DIV");
    columna.classList.add("col-6");
    columna.classList.add("col-lg-3");
    columna.classList.add("p-3");
    columna.innerHTML += `<div class="container-fluid">
                            <div class="container-fluid border border-2 bg-body trivia">
                              <img src="../img/pikachu_bailando.gif" alt=${i} class="img-${i} img-fluid w-100">
                            </div>
                          </div>`;
    fragmento.appendChild(columna);
  }
  contenedor.appendChild(fragmento);
  mostrarResultado();
}

const mostrarResultado = () => {
  const contenedor = document.querySelector(".resultado");
  contenedor.innerHTML = `<div class="row h-75 d-flex justify-content-center p-3">
                            <div class="rounded-3 bg-body h-100">
                              <p class="textOut text-center">Felicidades<br>Su puntaje fue ${cntAciertos}</p>
                            </div>
                          </div>
                          <div class="row h-25">
                            <div class="col-6 position-relative">
                              <a class="w-75 x-3 btn btn-warning position-absolute top-50 start-50 translate-middle" href="../index.html" role="button"><b>Volver</b></a>
                            </div>
                            <div class="col-6 position-relative">
                            <a class="w-75 x-3 btn btn-warning position-absolute top-50 start-50 translate-middle" href="triviaPage.html" role="button"><b>Reiniciar</b></a>
                            </div>
                          </div>`;
  
}

const cargarPagina = () => {
  if(cntIntentos == 15) {
    cargarSuccess();
  }
  else {
    boton.classList.add("disabled");
    cargarImagenes();
    cntIntentos++; 
  }
} 

window.addEventListener("load", () => {
  boton.addEventListener("click", () => {cargarPagina();});
  cargarPagina();
});

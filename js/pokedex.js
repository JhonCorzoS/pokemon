function contruccion() {
  const id = Number(new URLSearchParams(location.search).get('id'));
  const API_URL = "https://pokeapi.co/api/v2/pokemon/" + id
  fetch(API_URL)
      .then(response => response.json())
      .then(data => {

          pintarDatosBasicos(id, data);
          pintarEstadisticas(data);
          cargarHabilidades(data);
          cargarEvoluciones(id);
      }
      )
      .catch(err => console.log(err))
}
async function cargarEvoluciones(id) {
  const URL = await descripcion(id)
  let impresion = ""
  const plantilla = `
  <div class="col-3 chainmar my-3 rounded p-3 mx-3">
      <div class="container bg-light p-3 rounded">
          <img class="img-fluid" src="@" alt="" srcset="">
          <h5 class="text-dark text-center" >!</h5>
      </div>
  </div>`
  let data = await fetch(URL["evolution_chain"]["url"]).then(response => response.json())
  // console.log(data)
  let name = data["chain"]["species"]["name"];
  // console.log(name)
  let img = await cargarimg(name);

  //console.log(name)
  img = img.sprites.other["official-artwork"]["front_default"]
  impresion += plantilla.replace("@", img).replace("!", name);

  //console.log(img)
  let posiblesEvo = data["chain"]["evolves_to"]
  if (posiblesEvo.length > 1) {
      for (let i = 0; i < posiblesEvo.length; i++) {
          name = posiblesEvo[i]["species"]["name"];
          img = await cargarimg(posiblesEvo[i]["species"]["name"]);
          img = img.sprites.other["official-artwork"]["front_default"];
          // console.log(name)
          // console.log(img)
          impresion += plantilla.replace("@", img).replace("!", name);
      }
  }
  else {
      let access = data["chain"];
      //recorrerChain(access);
      // console.log(access)  
      let str = recorrerChain(access)
      str = str.split("/")
      for (let i = 0; i < str.length; i++) {
          if (str[i] != "") {
              name = str[i]
              img = await cargarimg(name);
              img = img.sprites.other["official-artwork"]["front_default"];
              impresion += plantilla.replace("@", img).replace("!", name);
          }

      }

  }
  document.querySelector("#chain").innerHTML = impresion
}

function recorrerChain(access) {
  if (access["evolves_to"].length == 0) {
      return "";
  }
  return access["evolves_to"][0]["species"]["name"] + "/" + recorrerChain(access["evolves_to"][0]);
}


function cargarimg(id) {
  const API_URL = "https://pokeapi.co/api/v2/pokemon/" + id
  return result = fetch(API_URL).then(result => result.json())

}
function cargarHabilidades(data) {
  const plantilla = '<div class="col-5 rounded-3 p-3"><div class="row"><div class="container bg-danger rounded-top  d-flex justify-content-center">@</div><div class="col-12 text-center p-3 text-dark rounded-bottom bg-light">!</div></div></div>'
  const habilidades = data["abilities"]
  let aux = ""
  for (let i = 0; i < habilidades.length; i++) {
      aux += plantilla.replace("@", habilidades[i]["ability"]["name"]);
      fetch(habilidades[i]["ability"]["url"])
          .then(response => response.json())
          .then(data => {
              aux = aux.replace("!", data["flavor_text_entries"][0]["flavor_text"])
              document.querySelector(".habilidades").innerHTML = aux;
          })

  }

}
function pintarEstadisticas(data) {
  const largo = "@%";
  const pintarHp = document.querySelector(".barrahp");
  const pintaratc = document.querySelector(".barraatc");
  const pintardef = document.querySelector(".barradefe");
  const pintarsa = document.querySelector(".specialAbar");
  const pintarsd = document.querySelector(".specialDbar");
  const pintarspd = document.querySelector(".speedbar");
  const stats = data.stats;
  pintarHp.style.width = largo.replace("@", stats[0]["base_stat"]);
  pintaratc.style.width = largo.replace("@", stats[1]["base_stat"]);
  pintardef.style.width = largo.replace("@", stats[2]["base_stat"]);
  pintarsa.style.width = largo.replace("@", stats[3]["base_stat"]);
  pintarsd.style.width = largo.replace("@", stats[4]["base_stat"]);
  pintarspd.style.width = largo.replace("@", stats[5]["base_stat"]);
  document.querySelector("#hp").innerHTML = stats[0]["base_stat"];
  document.querySelector("#atc").innerHTML = stats[1]["base_stat"];
  document.querySelector("#def").innerHTML = stats[2]["base_stat"];
  document.querySelector("#sa").innerHTML = stats[3]["base_stat"];
  document.querySelector("#sd").innerHTML = stats[4]["base_stat"];
  document.querySelector("#sp").innerHTML = stats[5]["base_stat"];

}



async function pintarDatosBasicos(id, data) {
  const formato = '<div class="col-4 rounded p-2 text-dark text-center my-2 mx-2" style="background-color:!;">@</div><br>';
  const formatoimg = '<img class="img-fluid" src="@" alt="" srcset="">';
  const pintarId = document.querySelector("#pokeId");
  const pintarNombre = document.querySelector("#nombrePokemon");
  const pintarTipos = document.querySelector("#tipos");
  const pintarDescripcion = document.querySelector("#descripcion");
  const pintarImg = document.querySelector("#img");
  let tipos = data.types;
  let aux = "";
  for (let index = 0; index < tipos.length; index++) {
      datosColor = coloresTipos(tipos[index].type.name);
      aux += formato.replace("@", datosColor.tipo);
      aux = aux.replace("!", datosColor.color)
  }
  pintarTipos.innerHTML = aux;
  pintarId.innerHTML = "No." + id;
  pintarNombre.innerHTML = data.name;
  pintarImg.innerHTML += formatoimg.replace("@", data.sprites.other["official-artwork"]["front_default"]);
  desc = await descripcion(id);
  pintarDescripcion.innerHTML = desc["flavor_text_entries"][0]["flavor_text"];
}

function descripcion(id) {
  const URL = `https://pokeapi.co/api/v2/pokemon-species/${id}/`
  return fetch(URL).then(res => res.json())
}
function coloresTipos(tipo) {
  const colores = [
      {
          "tipo": "normal",
          "color": "#DDCCAA"
      },
      {
          "tipo": "fighting",
          "color": "#FF6A6A"
      },
      {
          "tipo": "flying",
          "color": "#BAAAFF"
      },
      {
          "tipo": "poison",
          "color": "#CC88BB"
      },
      {
          "tipo": "ground",
          "color": "#DEB887"
      },
      {
          "tipo": "rock",
          "color": "#CD853F"
      },
      {
          "tipo": "bug",
          "color": "#99CC33"
      },
      {
          "tipo": "ghost",
          "color": "#778899"
      },
      {
          "tipo": "steel",
          "color": "#CCCCCC"
      },
      {
          "tipo": "fire",
          "color": "#FF7F00"
      },
      {
          "tipo": "water",
          "color": "#B0E2FF"
      },
      {
          "tipo": "grass",
          "color": "#99FF66"
      },
      {
          "tipo": "electric",
          "color": "#FFD700"
      },
      {
          "tipo": "psychic",
          "color": "#FFB5C5"
      },
      {
          "tipo": "ice",
          "color": "#ADD8E6"
      },
      {
          "tipo": "dragon",
          "color": "#AB82FF"
      },
      {
          "tipo": "dark",
          "color": "#A9A9A9"
      },
      {
          "tipo": "fairy",
          "color": "#FFB0FF"
      },
  ]
  for (let i = 0; i < colores.length; i++) {
      if (tipo == colores[i].tipo) {
          return colores[i]
      }

  }
}